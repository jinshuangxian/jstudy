package cn.spream.jstudy.curator;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.atomic.AtomicValue;
import org.apache.curator.framework.recipes.atomic.DistributedAtomicLong;
import org.apache.curator.framework.recipes.cache.ChildData;
import org.apache.curator.framework.recipes.cache.PathChildrenCache;
import org.apache.curator.framework.recipes.leader.LeaderSelector;
import org.apache.curator.framework.recipes.leader.LeaderSelectorListener;
import org.apache.curator.framework.state.ConnectionState;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.data.Stat;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 14-10-17
 * Time: 下午 5:28
 * To change this template use File | Settings | File Templates.
 */
public class TestCurator {

    private String connectString = "127.0.0.1:2181";
    private String path = "/jstudy/curator";
    private CuratorFramework curatorFramework;

    @Before
    public void init() throws Exception {
        //1.通过newClient创建client
//        curatorFramework = CuratorFrameworkFactory.newClient(connectString, new ExponentialBackoffRetry(1000, 3));
        //2.通过builder创建client
        curatorFramework = CuratorFrameworkFactory.builder()
                .connectString(connectString)
                .retryPolicy(new ExponentialBackoffRetry(1000, 3))
                .build();
        curatorFramework.start();
        //没有path则创建path
        Stat stat = curatorFramework.checkExists().forPath(path);
        if (stat == null) {
            curatorFramework.create().forPath(path);
        }
    }

    /**
     * 创建子节点
     *
     * @throws Exception
     */
    @Test
    public void createChildData() throws Exception {
        curatorFramework.create().forPath(path + "/child", "hello curator".getBytes());
    }

    /**
     * 删除子节点
     *
     * @throws Exception
     */
    @Test
    public void deleteChildData() throws Exception {
        curatorFramework.delete().forPath(path + "/child");
    }

    /**
     * patch缓存
     *
     * @throws Exception
     */
    @Test
    public void testCache() throws Exception {
        //对指定节点进行缓存并监听，无需关注节点的变化
        PathChildrenCache pathChildrenCache = new PathChildrenCache(curatorFramework, path, true);
        pathChildrenCache.start(PathChildrenCache.StartMode.BUILD_INITIAL_CACHE);
        List<ChildData> childDatas = pathChildrenCache.getCurrentData();
        for (ChildData childData : childDatas) {
            String path = childData.getPath();
            String data = new String(childData.getData());
            System.out.println(path + "-->" + data);
        }
    }

    /**
     * 分布式计数器
     *
     * @throws Exception
     */
    @Test
    public void testCounter() throws Exception {
        DistributedAtomicLong distributedAtomicLong = new DistributedAtomicLong(curatorFramework, path + "/count", new ExponentialBackoffRetry(1000, 3));
        AtomicValue<Long> atomicValue = distributedAtomicLong.increment();
        System.out.println("success:" + atomicValue.succeeded() + ";before:" + atomicValue.preValue() + ";after:" + atomicValue.postValue());
    }

    /**
     * leader选举
     *
     * @throws InterruptedException
     */
    @Test
    public void testLeader() throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(20);
        //启动多个线程模拟客户端，每个线程随机占用一段时间的leader权限然后释放leader权限
        for (int i = 1; i <= 6; i++) {
            final String name = "client-" + i;
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    LeaderSelector leaderSelector = new LeaderSelector(curatorFramework, path + "/leader", new LeaderSelectorListener() {

                        @Override
                        public void takeLeadership(CuratorFramework client) throws Exception {
                            //获取leader权限
                            System.out.println(name + "获取leader权限");
                            int workTime = new Random().nextInt(10);
                            TimeUnit.SECONDS.sleep(workTime);
                            System.out.println(name + "放弃leader权限,work time=" + workTime);
                        }

                        @Override
                        public void stateChanged(CuratorFramework client, ConnectionState newState) {

                        }

                    });
                    leaderSelector.start();
                }
            });
        }
        TimeUnit.SECONDS.sleep(60);
    }

    /**
     * 分布式锁
     * @throws InterruptedException
     */
    @Test
    public void testLock() throws InterruptedException {
        int count = 20;
        ExecutorService executorService = Executors.newFixedThreadPool(count);
        for (int i = 1; i <= count; i++) {
            final String name = "client-" + i;
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    DistributedLock distributedLock = new DistributedLock(curatorFramework, path + "/lock");
                    System.out.println(name + "等待锁...");
                    distributedLock.lock();
                    System.out.println(name + "获取锁");
                    int workTime = new Random().nextInt(10);
                    try {
                        TimeUnit.SECONDS.sleep(workTime);
                    } catch (InterruptedException e) {
                        new RuntimeException(e);
                    } finally {
                        System.out.println(name + "释放锁,work time=" + workTime);
                        distributedLock.unlock();
                    }
                }
            });
        }
        DistributedLock distributedLock = new DistributedLock(curatorFramework, path + "/lock");
        if (distributedLock.tryLock()) {
            System.out.println("其他线程尝试获取锁成功");
            distributedLock.unlock();
        } else {
            System.out.println("其他线程尝试获取锁失败");
        }
        TimeUnit.SECONDS.sleep(10 * count);
    }

    @After
    public void destroy() throws Exception {
        curatorFramework.close();
    }

}

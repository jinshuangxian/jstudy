package cn.spream.jstudy.thread;

import org.junit.Test;

import java.util.concurrent.*;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 14-7-19
 * Time: 下午3:39
 * To change this template use File | Settings | File Templates.
 */
public class TestFuture {

    @Test
    public void test() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        FutureTask<String> futureTask = new FutureTask<String>(new Callable<String>() {
            @Override
            public String call() throws Exception {
                Thread.sleep(5000);
                return "远程加载数据";
            }
        });
        System.out.println("准备提交耗时任务");
        executorService.execute(futureTask);
        System.out.println("提交耗时任务成功");
        try {
            System.out.println("等待耗时任务执行");
            String result = futureTask.get();
            System.out.println("耗时任务执行完成：" + result);
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (ExecutionException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

}

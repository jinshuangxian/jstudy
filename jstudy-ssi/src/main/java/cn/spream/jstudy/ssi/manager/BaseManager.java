package cn.spream.jstudy.ssi.manager;

import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * Created by IntelliJ IDEA.
 * User: sjx
 * Date: 13-5-20
 * Time: 下午12:57
 * To change this template use File | Settings | File Templates.
 */
public class BaseManager {

    private PlatformTransactionManager transactionManager;

    protected TransactionTemplate getTransactionManager() {
        return new TransactionTemplate(transactionManager);
    }

    public void setTransactionManager(PlatformTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }
}
